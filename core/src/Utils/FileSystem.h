#ifndef FILESYSTEM_H_
#define FILESYSTEM_H_

#include <string>

namespace fs
{

std::string currentWorkingDir();
const char nativeSeparator();

}

#endif // !FILESYSTEM_H_
