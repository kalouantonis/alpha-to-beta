#ifndef INPUT_LOCATOR_H
#define INPUT_LOCATOR_H

#include <Resources/Locator.h>
#include <Input/InputProcessor.h>

class InputLocator: public Locator<InputProcessor>
{
	// Game class is the only one that can call private methods
	// Remember kids, only friends have access to your privates...
	friend class Game;

private:
	static void dispatchEvent(const sf::Event& event);
};

/**
 * @brief Create input processor that does not belong to locator.
 * Simply, the locator will not delete the object
 * @param p
 * @return
 */
//InputLocator::Ptr owned_processor(InputProcessor* p);

#endif // INPUT_LOCATOR_H
